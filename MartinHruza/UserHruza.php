<?php



final class UserHruza {

    /** Vrátí zvuk
     * @author Martin Hruza
     * @param string $zvuk
     * @return string
     */
    public function reproduktor(string $zvuk): string {

        $reproduktor_zvuk = $this->ts->reproduktor($zvuk);

        /**
         * komentar
         * nebo viceradkovy
         */
        return $reproduktor_zvuk;
    }

    private $username = 'Martin';
    private $password = '1234';
    protected $firstName = 'Martin';
    protected $lastName = 'Hruza';
    protected $phone = 123456789;
    public $street = 'Tupolevova 11';
    public $zip = 27386; // psc
    public $invoiceId = 0123574112; // ico
    public $loginCount = 0;
    public $bornDate = 2002;

    public function __construct() {
        
    }

    /**
     * komentar
     * @param string $username
     */
    function setUsernameHruza(string $username) {
        $this->username = $username;
    }
    /**
     * komentar
     * @param string $password
     */
    function setPasswordHruza(string $password) {
        $this->password = $password;
    }
    /**
     * komentar
     * @return string
     */
    function getStreetHruza(): string {
        return $this->street;
    }
    /**
     * komentar
     * @return int
     */
    function getZipHruza(): int {
        return $this->zip;
    }
    /**
     * komentar
     * @return int
     */
    function getInvoiceIdHruza(): int {
        return $this->invoiceId;
    }
    /**
     * komentar
     * @return int
     */
    function getLoginCountHruza(): int { 
        return $this->loginCount;
    }
    /**
     * komentar
     * @return int
     */
    function getBornDateHruza(): int {
        return $this->bornDate;
    }

}

$Martin = new UserHruza;
$Martin->setUsernameHruza("Martin");

echo $Martin->getStreetHruza();
echo $Martin->getZipHruza();
echo $Martin->getInvoiceIdHruza();
echo $Martin->getLoginCountHruza();
echo $Martin->getBornDateHruza();


echo var_dump($Martin);

